package org.leos.apache.testing;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;

/**
 * @author Crunchify.com
 * 
 */

public class RestGetExample {

	// Basic Get Example -> API real $
	@Test
	public void basicGetExample() throws Exception {
		// create HTTP Client
		HttpClient httpClient = HttpClientBuilder.create().build();
		// Create new getRequest with below mentioned URL
		HttpGet getRequest = new HttpGet("https://jsonplaceholder.typicode.com/todos/1");
		// Add additional header to getRequest which accepts application/xml data
		getRequest.addHeader("accept", "application/json");
		// Execute your request and catch response
		HttpResponse response = httpClient.execute(getRequest);
		// Check for HTTP response code: 200 = success
		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
		}
		// Get-Capture Complete application/xml body response
		BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
		String output;
		System.out.println("============Output:============");
		// Simply iterate through XML response and show on console.
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}

	}

	// GET WITH parameters -> http://example.com/?parts=all&action=finish    !!!!!!!!!!!!!
	@Test
	public void strPostParameterExample() throws Exception {
		// create HTTP Client
		HttpClient httpClient = HttpClientBuilder.create().build();
		// Create new getRequest with below mentioned URL
		HttpGet getRequest = new HttpGet("http://example.com/");
		URI uri = new URIBuilder(getRequest.getURI()).addParameter("parts", "all").addParameter("action", "finish")
				.build();
		((HttpRequestBase) getRequest).setURI(uri);
		HttpResponse response = httpClient.execute(getRequest);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		while ((line = rd.readLine()) != null) {
			System.out.println(line);
		}
	}
}