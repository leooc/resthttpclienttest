package org.leos.apache.testing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.junit.Test;

@SuppressWarnings("deprecation")
public class RestPostExample {
	
	private HttpClient client;

	//Post with authorization
	@Test
	public void whenPostRequestUsingHttpClient_thenCorrect() 
	  throws ClientProtocolException, IOException {
	    CloseableHttpClient client = HttpClients.createDefault();
	    HttpPost httpPost = new HttpPost("http://www.example.com");
	 
	    List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("username", "John"));
	    params.add(new BasicNameValuePair("password", "pass"));
	    httpPost.setEntity(new UrlEncodedFormEntity(params));
	 
	    CloseableHttpResponse response = client.execute(httpPost);
	    System.out.println("response -> " + response.getStatusLine().getStatusCode());
	    client.close();
	}
	
	//Post with authorization 2
	@Test
	public void whenPostRequestWithAuthorizationUsingHttpClient_thenCorrect()
	  throws ClientProtocolException, IOException, AuthenticationException {
	    CloseableHttpClient client = HttpClients.createDefault();
	    HttpPost httpPost = new HttpPost("http://www.example.com");
	 
	    httpPost.setEntity(new StringEntity("test post"));
	    UsernamePasswordCredentials creds
	      = new UsernamePasswordCredentials("John", "pass");
	    httpPost.addHeader(new BasicScheme().authenticate(creds, httpPost, null));
	 
	    CloseableHttpResponse response = client.execute(httpPost);
	    System.out.println("response -> " + response.getStatusLine().getStatusCode());
	    client.close();
	}
	
	//POST with JSON
	@Test
	public void whenPostJsonUsingHttpClient_thenCorrect() 
	  throws ClientProtocolException, IOException {
	    CloseableHttpClient client = HttpClients.createDefault();
	    HttpPost httpPost = new HttpPost("http://www.example.com");
	    StringEntity entity = new StringEntity("{\"id\":100,\"name\":\"iPad 4\"}");
	    httpPost.setEntity(entity);
	    httpPost.setHeader("Accept", "application/json");
	    httpPost.setHeader("Content-type", "application/json");
	 
	    CloseableHttpResponse response = client.execute(httpPost);
	    System.out.println("response -> " + response.getStatusLine().getStatusCode());
	    client.close();
	}
	
	//POST WITH json object
	@Test
	public void strSimplePostExample() throws Exception {
		client = new DefaultHttpClient();
		HttpPost post = new HttpPost("http://restUrl");
		JSONObject json = new JSONObject();
		json.put("name1", "value1");
		json.put("name2", "value2");
		StringEntity se = new StringEntity( json.toString());
		post.setEntity(se);
		HttpResponse response = client.execute(post);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		while ((line = rd.readLine()) != null) {
			System.out.println(line);
		}
	}
	
	// POST WITH parameters -> http://example.com/?parts=all&action=finish
	@Test
	public void strPostParameterExample() throws Exception {
		client = new DefaultHttpClient();
		HttpRequestBase request = new HttpGet("http://example.com/");
		URIBuilder builder = new URIBuilder(request.getURI().toString());
		builder.setParameter("parts", "all").setParameter("action", "finish");
		HttpPost post = new HttpPost(builder.build());
		HttpResponse response = client.execute(post);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		while ((line = rd.readLine()) != null) {
			System.out.println(line);
		}
	}
	
}
